@extends('layouts.main1')
@section('content')
            <form method="post" action="{{route('products.store')}}">
                @csrf
                <div class="form-group">
                    <label for="title">Mahsulot nomi:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label for="title">Mahsulot kodi:</label>
                    <input type="text" class="form-control" name="code"/>
                </div>
                <button type="submit" class="btn btn-primary">Saqlash</button>
            </form>
@endsection
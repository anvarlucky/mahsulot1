<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\BaseControllerForClient;
use Illuminate\Http\Request;

class MaterialController extends BaseControllerForClient
{
    public function index(){
        $materials = $this->get('http://mahsulot.loc/api/materials');
        return view('materials.index',[
            'materials' => $materials->data
        ]);
    }

    public function create(){
        return view('materials.create');
    }

    public function store(Request $request){
        $request = $this->post('http://mahsulot.loc/api/materials',$request->except('_token'));
        if ($request == true){
            return redirect()->route('materials.index');
        }
    }

    public function destroy($id){
        $material = $this->delete('http://mahsulot.loc/api/materials/'.$id);
        if ($material == true){
            return redirect()->route('materials.index');
        }
    }
}

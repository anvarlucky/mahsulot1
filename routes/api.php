<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\ProductController;
use App\Http\Controllers\Api\v1\MaterialController;
use App\Http\Controllers\Api\v1\WarehouseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('products',ProductController::class);
Route::apiResource('materials',MaterialController::class);
Route::apiResource('warehouses',WarehouseController::class)->except('show');
Route::get('warehouses/products/{product_id}/{amount}',[WarehouseController::class,'products']);

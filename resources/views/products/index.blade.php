@extends('layouts.main1')
@section('content')
    <h2 align="center">Mahsulotlar</h2><a href="{{route('products.create')}}">Yangi qo'shish</a>
    <table class="table table-sm my-2">
        <thead class="thead-dark">
        <tr>
            <td>№</td>
            <td>Nomi</td>
            <td>Kodi</td>
            <td>O'chrish</td>
        </tr>
        </thead>
        <tbody>
@foreach($products as $key => $product)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$product->name}}</td>
        <td>{{$product->code}}</td>
        <td>
            <form action="{{route('products.destroy', $product->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="btn-group btn-group-sm" role="group"><button class="btn btn-danger" type="submit" onclick="return confirm('{{$product->name}}')">O'chirish</button></div>
            </form>
            </td>
    </tr>
@endforeach
        </tbody>
    </table>
@endsection


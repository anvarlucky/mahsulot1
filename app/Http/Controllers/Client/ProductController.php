<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\BaseControllerForClient;
use Illuminate\Http\Request;

class ProductController extends BaseControllerForClient
{
    public function index(){
        $products = $this->get('http://mahsulot.loc/api/products');
        return view('products.index',[
            'products' => $products->data
            ]);
    }

    public function create(){
        return view('products.create');
    }

    public function store(Request $request){
        $request = $this->post('http://mahsulot.loc/api/products',$request->except('_token'));
        if ($request == true){
            return redirect()->route('products.index');
        }
    }

    public function destroy($id){
        $product = $this->delete('http://mahsulot.loc/api/products/'.$id);
        if ($product == true){
            return redirect()->route('products.index');
        }
    }
}

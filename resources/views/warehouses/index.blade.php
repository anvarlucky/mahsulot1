@extends('layouts.main1')
@section('content')
    <h2 align="center">Omborlar</h2><a href="{{route('warehouses.create')}}">Yangi qo'shish</a>
    <table class="table table-sm my-2">
        <thead class="thead-dark">
        <tr>
            <td>№</td>
            <td>Material</td>
            <td>Soni</td>
            <td>Narxi</td>
            <td>O'chrish</td>
        </tr>
        </thead>
        <tbody>
@foreach($warehouses as $key => $warehouse)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$warehouse->material_id}}</td>
        <td>{{$warehouse->remainder}}</td>
        <td>{{$warehouse->price}}</td>
        <td>
            <form action="{{route('warehouses.destroy', $warehouse->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="btn-group btn-group-sm" role="group"><button class="btn btn-danger" type="submit" onclick="return confirm('{{$warehouse->id}}')">O'chirish</button></div>
            </form>
            </td>
    </tr>
@endforeach
        </tbody>
    </table>
@endsection


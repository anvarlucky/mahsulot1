<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseControllerForApi;
use Illuminate\Http\Request;
use App\Models\v1\Material;

class MaterialController extends BaseControllerForApi
{
    public function index(){
        $materials = Material::all();
        return $this->responseSuccess($materials);
    }

    public function store(Request $request){
        $material = new Material();
        $material->name = $request->name;
        $material->save();
        if ($material == true){
            return $this->responseSave($material);
        }
    }

    public function destroy($id){
        $material = Material::destroy($id);
        if ($material == true){
            return $this->responseDelete($material);
        }
    }
}

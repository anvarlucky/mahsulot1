<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseControllerForApi;
use Illuminate\Http\Request;
use App\Models\v1\Product;

class ProductController extends BaseControllerForApi
{
    public function index(){
        $products = Product::all();
        return $this->responseSuccess($products);
    }

    public function store(Request $request){
        $product = new Product();
        $product->name = $request->name;
        $product->code = $request->code;
        $product->save();
        if ($product == true){
            return $this->responseSuccess($product);
        }
    }

    public function destroy($id){
        $product = Product::destroy($id);
        return $this->responseDelete($product);
    }
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\BaseControllerForApi;
use App\Models\v1\Product;
use App\Models\v1\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends BaseControllerForApi
{
    public function index(){
        $warehouses = Warehouse::all();
        return $this->responseSuccess($warehouses);
    }

    public function products($product_id,$amount){
        $product = Warehouse::getByProduct($product_id,$amount);
        return $this->responseSuccess($product);

    }

    public function store(Request $request){
        $warehouse = new Warehouse();
        $warehouse->material_id = $request->material_id;
        $warehouse->remainder = $request->remainder;
        $warehouse->price = $request->price;
        $warehouse->save();
        if ($warehouse ==true){
            return $this->responseSave($warehouse);
        }
    }

    public function destroy($id){
        $warehouse = Warehouse::destroy($id);
        if ($warehouse == true){
            return $this->responseDelete($warehouse);
        }
    }
}

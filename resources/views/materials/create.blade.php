@extends('layouts.main1')
@section('content')
            <form method="post" action="{{route('materials.store')}}">
                @csrf
                <div class="form-group">
                    <label for="title">Xomashyo nomi:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <button type="submit" class="btn btn-primary">Saqlash</button>
            </form>
@endsection
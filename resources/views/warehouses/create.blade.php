@extends('layouts.main1')
@section('content')
            <form method="post" action="{{route('warehouses.store')}}">
                @csrf
                <div class="form-group">
                    <label for="material_id">Mahsulotni tanlang:</label>
                    <select name="material_id" class="form-control">
                        <option value=""></option>
                        @foreach($materials as $material)
                            <option value="{{$material->id}}">{{$material->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Partiyadagi Xomashyo qoldig`i:</label>
                    <input type="text" class="form-control" name="remainder"/>
                </div>
                <div class="form-group">
                    <label for="title">Partiyadagi xomashyo narxi:</label>
                    <input type="text" class="form-control" name="price"/>
                </div>
                <button type="submit" class="btn btn-primary">Saqlash</button>
            </form>
@endsection
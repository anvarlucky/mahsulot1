<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_materials')->insert([
            'product_id' => 1,
            'material_id' => 1,
            'quantity' => 0.8,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_materials')->insert([
            'product_id' => 1,
            'material_id' => 3,
            'quantity' => 5,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_materials')->insert([
            'product_id' => 1,
            'material_id' => 2,
            'quantity' => 10,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('product_materials')->insert([
            'product_id' => 2,
            'material_id' => 1,
            'quantity' => 1.4,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_materials')->insert([
            'product_id' => 2,
            'material_id' => 2,
            'quantity' => 10,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_materials')->insert([
            'product_id' => 2,
            'material_id' => 4,
            'quantity' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}

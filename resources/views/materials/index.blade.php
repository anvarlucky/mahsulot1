@extends('layouts.main1')
@section('content')
    <h2 align="center">Xomashyolar</h2><a href="{{route('materials.create')}}">Yangi qo'shish</a>
    <table class="table table-sm my-2">
        <thead class="thead-dark">
        <tr>
            <td>№</td>
            <td>Nomi</td>
            <td>O'chrish</td>
        </tr>
        </thead>
        <tbody>
@foreach($materials as $key => $material)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$material->name}}</td>
        <td>
            <form action="{{route('materials.destroy', $material->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="btn-group btn-group-sm" role="group"><button class="btn btn-danger" type="submit" onclick="return confirm('{{$material->name}}')">O'chirish</button></div>
            </form>
            </td>
    </tr>
@endforeach
        </tbody>
    </table>
@endsection


<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert([
            'name' => 'Mato',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('materials')->insert([
            'name' => 'Ip',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('materials')->insert([
            'name' => 'Tugma',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('materials')->insert([
            'name' => 'Zamok',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}

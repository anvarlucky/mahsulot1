<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Warehouse extends Model
{
    use HasFactory;

    public static function getByProductTest($product_id){
        $product = DB::table('warehouses')
            ->leftJoin('materials','warehouses.material_id','=','materials.id')
            ->select('materials.name as material_name','warehouses.id as warehouse_id','warehouses.price')
            ->get();
        return $product;
    }

    public static function getByProduct($product_id,$amount){
        $warehouse = DB::table('warehouses')
            ->leftJoin('product_materials','product_materials.material_id','=','warehouses.material_id')
            ->leftJoin('materials','warehouses.material_id','=','materials.id')
            ->select('warehouses.id as warehouse_id','warehouses.remainder as qty','warehouses.price as price','materials.name as material_name')
            ->where(['product_materials.product_id' => $product_id])
            ->get();
        $productMaterial = DB::table('product_materials')
            ->leftJoin('products','product_materials.product_id','=','products.id')
            ->leftJoin('materials','product_materials.material_id','=','materials.id')
            ->select('materials.name as material_name','product_materials.quantity as qty')
            ->where('products.id','=',$product_id)
            ->get();
        $product = DB::table('products')
            ->select('products.name as product_name')
            ->where('products.id','=',$product_id)
            ->get();
        //Count

        $productMaterialCount = DB::table('product_materials')->select('quantity')
            ->where(['material_id'=>1,'product_id'=>$product_id])
            ->sum('quantity');
        $productMaterialCount2 = DB::table('product_materials')->select('quantity')
            ->where(['material_id'=>2,'product_id'=>$product_id])
            ->sum('quantity');
        $productMaterialCount3 = DB::table('product_materials')->select('quantity')
            ->where(['material_id'=>3,'product_id'=>$product_id])
            ->sum('quantity');
        $productMaterialCount4 = DB::table('product_materials')->select('quantity')
            ->where(['material_id'=>4,'product_id'=>$product_id])
            ->sum('quantity');
        return ['result' =>$product,'product_materials' => $warehouse,'Mato' => $productMaterialCount*$amount, 'Ip' => $productMaterialCount2*$amount,
            'Tugma' => $productMaterialCount3*$amount, 'Zamok' => $productMaterialCount4*$amount];
    }
}

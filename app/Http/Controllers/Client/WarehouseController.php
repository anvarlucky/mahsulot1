<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\BaseControllerForClient;
use Illuminate\Http\Request;

class WarehouseController extends BaseControllerForClient
{
    public function index(){
        $warehouses = $this->get('http://mahsulot.loc/api/warehouses');
        return view('warehouses.index',[
            'warehouses' => $warehouses->data
        ]);
    }

    public function create(){
        $materials = $this->get('http://mahsulot.loc/api/materials');
        return view('warehouses.create',[
            'materials' => $materials->data
        ]);
    }

    public function store(Request $request){
        $request = $this->post('http://mahsulot.loc/api/warehouses',$request->except('_token'));
        if ($request == true){
            return redirect()->route('warehouses.index');
        }
    }

    public function destroy($id){
        $warehouses = $this->delete('http://mahsulot.loc/api/warehouses/'.$id);
        if ($warehouses == true){
            return redirect()->route('warehouses.index');
        }
    }
}
